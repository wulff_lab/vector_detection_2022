# -*- coding: utf-8 -*-
"""
Created on Wed Sep  8 18:09:02 2021

@author: User
"""

import os
import json
import pandas as pd
import matplotlib.pyplot as plt

from config import output_dir

experiment = 'shape_detection'
hyperparameters = ['batch_size','lr','epochs']
datatypes = ['int32','float','int32']

rename_tablecells = {'precision': 'p', 'recall': 'r', 'f1-score': 'F1',
                     'micro avg': 'Micro', 'macro avg': 'Macro', 'weighted avg': 'Weighted',
                     'samples avg': 'Sample'}

exp_path = os.path.join( output_dir, experiment )
file_paths = [ os.path.join( exp_path, file ) for file in os.listdir( exp_path ) if file.endswith('.json') ]

values = pd.DataFrame()    
for filepath in file_paths:
    temp = json.load( open( filepath, 'r' ) )

    batch_size, lr, epochs = filepath.replace('.json','').split('_')[-3:]

    f1 = temp['weighted avg']['f1-score']                
    add = pd.DataFrame({
        'epochs': [epochs],
        'batch_size': [batch_size], 
        'lr': [lr], 
        'f1': [f1] })
    add = add.astype({ key:val for key,val in zip(hyperparameters,datatypes)})
    values = pd.concat([values,add])
    
print( values.sort_values(['f1']) )

# plot f1 values:
fig,ax = plt.subplots(3,3,sharex=False,sharey=False)
for i,para1 in enumerate(hyperparameters):
    for j,para2 in enumerate(hyperparameters):
        ax[i,j].scatter( values[para1], values[para2], c=values['f1'], cmap="RdYlGn", s = 10 )
        # if i==j:
        #     ax[i,j].text(-2,1, '{:.0f}'.format(corrs[i]*100) )
plt.show()

exit()

"""
new_dtypes = { key: value for key, value in zip(values.columns, column_types) }
values = values.astype(new_dtypes)
    

# load best file:
table = pd.DataFrame()    


best_values_reordered = [ best_values( exp )[1].__getitem__(x) for x in [4,0,1,2,3] ]
paras = '_'.join( [ para.split(': ')[1] for para in best_values_reordered ] )
experiment_name = experiments_dict[ exp ]

temp = json.load( 
    open( os.path.join( filespath, experiment_name + '_5dim_' + paras ), 'r' ) )
support = pd.DataFrame( temp ).copy(deep=True)
support = support.iloc[3,:].T.astype(int)


temp = np.round( pd.DataFrame( temp ).iloc[:3,:].T, 2 )

table = pd.concat([table,temp], axis = 1 )
"""