# -*- coding: utf-8 -*-
"""
Created on Sat Sep  4 11:51:11 2021

@author: User
"""

import os
from config import data_dir, batch_size, lr, epochs, labels, device, output_dir, models_dict
import joblib
from pathlib import Path

import torch
from torch import nn
from torch.optim import Adam
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import DataLoader, RandomSampler, SequentialSampler, TensorDataset, Dataset

import torchvision
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
from torchvision.models.detection.mask_rcnn import MaskRCNNPredictor

from scipy import sparse
from sklearn.metrics import classification_report

import transforms as T
from typing import Union, Optional, List, Tuple

import pickle
import pandas as pd
import numpy as np

from PIL import Image, ImageDraw, ImageFont
from tqdm import tqdm

import random
import argparse

from torchvision import models as models
from sklearn.model_selection import train_test_split

def get_model_instance_segmentation(num_classes,pretrained):
    model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=True)
    
    # get number of input features for the classifier
    in_features = model.roi_heads.box_predictor.cls_score.in_features
    # replace the pre-trained head with a new one
    model.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_classes)

    return model

def collate_fn(batch):
    return tuple(zip(*batch))

def map_normalize_degrees(degree):
    return np.round( (1/180)*degree - 1, 2 )

def shifty(xs, n):
    e = np.ones(xs.shape)*255
    if n >= 0:
        e[n:,:,:] = xs[:-n,:,:]
    else:
        e[:n,:,:] = xs[-n:,:,:]
    return e

def shiftx(xs, n):
    e = np.ones(xs.shape)*255
    if n >= 0:
        e[:,n:,:] = xs[:,:-n,:]
    else:
        e[:,:n,:] = xs[:,-n:,:]
    return e

def seed_everything(SEED=42):
    random.seed(SEED)
    np.random.seed(SEED)
    torch.manual_seed(SEED)
    torch.cuda.manual_seed(SEED)
    torch.cuda.manual_seed_all(SEED)
    torch.backends.cudnn.benchmark = True 

def dataloader( dataset='train', model = 'CNN', label='one_hot' ):
    """
    Parameters
    ----------
    label str
        options: 'one_hot' -> all labels as dummies, LABELNAME -> label as first column, second colum is other

    Returns
    -------
    DataLoader
        DESCRIPTION.

    """
    # load data:
    data = pickle.load( open( os.path.join( data_dir, dataset + '_data.pickle' ), 'rb' ) )
    X,y = zip(*data)
    
    dataset_cls=TensorDataset
    sampler_cls=RandomSampler
    
    # TRANSFORM X values:
    # unpack sparse matrices:
    if model=='CNN':
        X = [ np.array( x.todense(), dtype=np.float32 ).reshape(1,150,200) for x in X ]
    elif model=='FFNN':
        X = [ np.array( x.todense(), dtype=np.float32 ).reshape( 30000, ) for x in X ]
    
    # transform y values:
    if label=='one_hot':
        y = pd.get_dummies( y )
        # fill missing labels, if existent:
        y = y.T.reindex(labels).T.fillna(0).values
    else:
        y = np.array( [ [1,0] if y_==label else [0,1] for y_ in y ] )
    
    # Tensors
    train_y_tensor = torch.tensor(y).float()
    train_x_tensor = torch.tensor(X)
   
    # creates typical google tokens [tokenization]
    train_dataset = dataset_cls(train_x_tensor, train_y_tensor)
    train_sampler = sampler_cls(train_dataset)
    
    return DataLoader(train_dataset, sampler=train_sampler, batch_size=batch_size)

def shapes_dataloader( dataset='train', batch_size=batch_size ):
    # read the data.csv file and get the image paths and labels
    df = pd.read_csv( os.path.join( data_dir, 'shapes_paper_Korchi2020', 'data.csv') )
    X = [ path.replace('.\\','') for path in df.image_path.values ]
    y = df.target.values

    (xtrain, xtest, ytrain, ytest) = (train_test_split(X, y, test_size=0.5, random_state=42))
    (xtest, xval, ytest, yval) = (train_test_split(xtest, ytest, test_size=0.5, random_state=42))
    
    if dataset=='train':
        train_data = ShapesDataset(xtrain, ytrain)
        return DataLoader(train_data, batch_size=batch_size, shuffle=True)
    elif dataset=='val':
        val_data = ShapesDataset(xval, yval)
        return DataLoader(val_data, batch_size=batch_size, shuffle=False)
    
class ShapesDataset(Dataset):
    def __init__(self,path,labels):
        self.X = path
        self.y = labels
        
    def __len__(self):
        return (len(self.X))
    
    def __getitem__(self, i):
        img = Image.open( os.path.join( data_dir, 'shapes_paper_Korchi2020', self.X[i]) )
        img = np.array( img, dtype = np.float32 )
        img = np.moveaxis(img,-1,0)
        img = torch.tensor( img )
        
        label = torch.tensor( self.y[i], dtype=int )
        
        return img, label

def vector_dataloader( dataset = 'train', batch_size=batch_size, experiment='vectors',
                       load_from_list = False, abs_size_val = None, training_size = None,
                       without_masks = False ):
    root = os.path.join(data_dir, experiment)
    if not load_from_list:
        imgs = [ file for file in os.listdir(root) \
                       if '.png' in file and \
                       not file.replace('.png','').endswith('mask') and \
                       not 'prototype' in file ]
    else:
        imgs = pd.read_csv( os.path.join( root, 'imagefile.csv'))
        imgs = imgs.image.values
    
    (xtrain, xtest) = (train_test_split(imgs, test_size=0.2, random_state=42))
    (xtest, xval) = (train_test_split(xtest, test_size=0.8, random_state=42))
    
    if training_size:
        xtrain = xtrain[:training_size]
    if abs_size_val:
        xval = xval[:abs_size_val]
    
    if dataset=='train':
        train_data = VectorDataset(xtrain, get_transform(train=True), experiment, without_masks = without_masks )
        return DataLoader(train_data,batch_size=batch_size, shuffle=True, collate_fn=collate_fn)
    elif dataset=='val':
        val_data = VectorDataset(xval, get_transform(train=False), experiment, without_masks = without_masks )
        return DataLoader(val_data,batch_size=batch_size, shuffle=False, collate_fn=collate_fn)

def get_transform(train):
    transforms = []
    transforms.append(T.ToTensor())
    # if train:
    #     transforms.append(T.RandomHorizontalFlip(0.5))
    return T.Compose(transforms)

class VectorDataset(torch.utils.data.Dataset):
    def __init__(self,imageslist, transforms, experiment, without_masks ):
        # load all image files, sorting them to
        # ensure that they are aligned
        self.root = os.path.join(data_dir, experiment )
        self.transforms = transforms
        self.without_masks = without_masks        
        
        self.imageslist = imageslist
        self.maskslist = [ f'{p.replace(".png","")}_mask.png' for p in imageslist ]

    def __getitem__(self, idx):
        # load images and masks
        img_path = os.path.join(self.root, self.imageslist[idx])
        img = Image.open(img_path).convert("RGB")
        
        """ with get_transform this is irrelevant
        img = np.array( img, dtype = np.float32 )
        img = np.moveaxis(img,-1,0)
        img = torch.tensor( img )
        """
        
        if self.without_masks:
            # this is for the experiment regression_vectors:
            # img = np.array( img, dtype = np.float32 )
            # img = np.moveaxis(img,-1,0)
            # img = torch.tensor( img )

            # retrieve direction of the vector:
            label = int( Path( img_path ).parts[-1].split('.')[0].split('_')[-1] )
            label = map_normalize_degrees( label )
            img, label = self.transforms(img, label)

            return img, label
            
        elif not self.without_masks:
            mask_path = os.path.join(self.root, self.maskslist[idx])
            # note that we haven't converted the mask to RGB,
            # because each color corresponds to a different instance
            # with 0 being background
            mask = Image.open(mask_path)
            
            # convert the PIL Image into a numpy array
            mask = np.array(mask)
    
            # instances are encoded as different colors
            obj_ids = np.unique(mask)
            
            # first id is the background, so remove it
            obj_ids = obj_ids[1:]
            
            # split the color-encoded mask into a set
            # of binary masks
            masks = mask == obj_ids[:, None, None]
            
            # get bounding box coordinates for each mask
            num_objs = len(obj_ids)
            boxes = []
            for i in range(num_objs):
                pos = np.where(masks[i])
                xmin = np.min(pos[1])
                xmax = np.max(pos[1])
                ymin = np.min(pos[0])
                ymax = np.max(pos[0])
                boxes.append([xmin, ymin, xmax, ymax])
            
            # convert everything into a torch.Tensor
            boxes = torch.as_tensor(boxes, dtype=torch.float32)
            # there is only one class
            labels = torch.ones((num_objs,), dtype=torch.int64)
            masks = torch.as_tensor(masks, dtype=torch.uint8)
    
            image_id = torch.tensor([idx], dtype=torch.int64)
            area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
                
            # suppose all instances are not crowd
            iscrowd = torch.zeros((num_objs,), dtype=torch.int64)
    
            target = {}
            target["boxes"] = boxes
            target["labels"] = labels
            target["masks"] = masks
            target["image_id"] = image_id
            target["area"] = area
            target["iscrowd"] = iscrowd
                    
            img, target = self.transforms(img, target)

            return img, target

    def __len__(self):
        return len(self.imageslist)
    

def train_model( model, dataloader, lr, epochs, loss_func = nn.CrossEntropyLoss() ):
    # training:
    paras = {'output_dim': 1}
    model = models_dict( model )( **paras )

    model.to( device )
    
    optimizer = Adam(model.parameters(), lr=lr)
    
    for epoch_num in range(epochs):
        model.train()
        train_loss = 0
        train_loss_plt = []
        
        print(f'Epoch: {epoch_num + 1}/{epochs}')
    
        for step_num, batch_data in enumerate(tqdm(dataloader, desc="Iteration")):
            
            X, y = batch_data
            
            # see if X is presented as a torch.tensor or a tuple(torch.tensors):
            if isinstance(X,list) or isinstance(X,tuple):
            
                if not isinstance(y,type(torch.tensor([0.]))):
                    y = [ torch.tensor(y).to(device) for y in y ]
                    
                X = [ x.to( device ) for x in X ]
                model_outputs = [ model( x.unsqueeze(dim=0) ) for x in X ]
                
                model_output = torch.stack(model_outputs ).squeeze()
                y = torch.stack( y )

                batch_loss = loss_func(model_output.float(), y.float() )

                train_loss += batch_loss.item()
                train_loss_plt += [train_loss]
        
                model.zero_grad()
                batch_loss.backward()
                optimizer.step()

            else:
                y.to(device)
                model_output = model(X)
                batch_loss = loss_func(model_output,y)
                
                train_loss += batch_loss.item()
                train_loss_plt += [train_loss]
        
                model.zero_grad()
                batch_loss.backward()
                optimizer.step()
        
        print(f'\r{epoch_num} loss: {train_loss / (step_num + 1)}')
    
    return model, train_loss_plt

def evaluate_model( model, dataloader, target_names=None, experiment=None ):
    model.eval()
    outputs,true_y = None,None
    
    with torch.no_grad():
        for step_num, batch_data in enumerate(tqdm(dataloader)):
            X, y = batch_data
            
            # see if X is presented as a torch.tensor or a tuple(torch.tensors):
            if isinstance(X,list) or isinstance(X,tuple):
                    
                X = [ x.to( device ) for x in X ]
                model_outputs = [ model( x.unsqueeze(dim=0) ) for x in X ]
                
                model_output = torch.stack( model_outputs ).squeeze()
                numpy_model_output = model_output.cpu().detach().numpy().reshape(-1,1)

                y = np.array( y ).reshape(-1,1)
                
            else:
                X = X.to( device )
                y = y.cpu().detach().numpy()
                
                model_output = model( X )
                numpy_model_output = model_output.cpu().detach().numpy()

            # collect outputs and true values in an array                
            if outputs is None:
                outputs = numpy_model_output
                true_y = y
            else:
                outputs = np.vstack((outputs, numpy_model_output))
                true_y = np.vstack((true_y, y))
       
    print(f'Evaluation completed for {len(outputs)} items')

    if 'shapes' in experiment:    
        true_y = true_y.reshape(1,-1)[0]
        
        # sanity check for vaules
        print( f'Output sums are: {outputs.sum(axis=0)}\n Output means are: {outputs.mean(axis=0)}' )
        
        os.path.join( data_dir, 'shapes_paper_Korchi2020', 'data.csv')
        pred_vals = np.round( np.apply_along_axis( lambda x: x==np.max(x), 1, outputs ) )
        pred_vals = list( map( np.argmax, pred_vals ) )
        
        print( classification_report(true_y, pred_vals, target_names=target_names ) )
        return (true_y,pred_vals)
    
    elif experiment=='vectors_directions':
        return(true_y,outputs)
    

def transform_image( name, model ):
    img = Image.open( os.path.join( data_dir, name + '.png' ) )
    
    if model.startswith('CNN_paper'):
        img = np.array( img, dtype = np.float32 )
        img = np.moveaxis(img,-1,0)
        img = torch.tensor( img )
        return img
    else:
        img = img.convert('RGBA')

        out = img.convert('1')
        
        out_array = np.array(out, dtype = int )
        
        x = -out_array + 1
        
        # unpack sparse matrices:
        if model=='CNN':
            X = np.array( x, dtype=np.float32 ).reshape(1,150,200)
        elif model=='FFNN':
            X = np.array( x, dtype=np.float32 ).reshape( 30000, )
        
        return torch.tensor( X ).unsqueeze(dim=0)


def draw_bounding_boxes(
    image: torch.Tensor,
    boxes: torch.Tensor,
    labels: Optional[List[str]] = None,
    colors: Optional[Union[List[Union[str, Tuple[int, int, int]]], str, Tuple[int, int, int]]] = None,
    fill: Optional[bool] = False,
    width: int = 1,
    font: Optional[str] = None,
    font_size: int = 10
) -> torch.Tensor:

    """
    source: https://pytorch.org/vision/master/_modules/torchvision/utils.html#draw_bounding_boxes
        
    Draws bounding boxes on given image.
    The values of the input image should be uint8 between 0 and 255.
    If fill is True, Resulting Tensor should be saved as PNG image.

    Args:
        image (Tensor): Tensor of shape (C x H x W) and dtype uint8.
        boxes (Tensor): Tensor of size (N, 4) containing bounding boxes in (xmin, ymin, xmax, ymax) format. Note that
            the boxes are absolute coordinates with respect to the image. In other words: `0 <= xmin < xmax < W` and
            `0 <= ymin < ymax < H`.
        labels (List[str]): List containing the labels of bounding boxes.
        colors (Union[List[Union[str, Tuple[int, int, int]]], str, Tuple[int, int, int]]): List containing the colors
            or a single color for all of the bounding boxes. The colors can be represented as `str` or
            `Tuple[int, int, int]`.
        fill (bool): If `True` fills the bounding box with specified color.
        width (int): Width of bounding box.
        font (str): A filename containing a TrueType font. If the file is not found in this filename, the loader may
            also search in other directories, such as the `fonts/` directory on Windows or `/Library/Fonts/`,
            `/System/Library/Fonts/` and `~/Library/Fonts/` on macOS.
        font_size (int): The requested font size in points.

    Returns:
        img (Tensor[C, H, W]): Image Tensor of dtype uint8 with bounding boxes plotted.
    """

    if not isinstance(image, torch.Tensor):
        raise TypeError(f"Tensor expected, got {type(image)}")
    elif image.dtype != torch.uint8:
        raise ValueError(f"Tensor uint8 expected, got {image.dtype}")
    elif image.dim() != 3:
        raise ValueError("Pass individual images, not batches")
    elif image.size(0) not in {1, 3}:
        raise ValueError("Only grayscale and RGB images are supported")

    if image.size(0) == 1:
        image = torch.tile(image, (3, 1, 1))

    ndarr = image.permute(1, 2, 0).numpy()
    img_to_draw = Image.fromarray(ndarr)

    img_boxes = boxes.to(torch.int64).tolist()

    if fill:
        draw = ImageDraw.Draw(img_to_draw, "RGBA")

    else:
        draw = ImageDraw.Draw(img_to_draw)

    txt_font = ImageFont.load_default() if font is None else ImageFont.truetype(font=font, size=font_size)

    for i, bbox in enumerate(img_boxes):
        if colors is None:
            color = None
        elif isinstance(colors, list):
            color = colors[i]
        else:
            color = colors

        if fill:
            if color is None:
                fill_color = (255, 255, 255, 100)
            elif isinstance(color, str):
                # This will automatically raise Error if rgb cannot be parsed.
                fill_color = ImageColor.getrgb(color) + (100,)
            elif isinstance(color, tuple):
                fill_color = color + (100,)
            draw.rectangle(bbox, width=width, outline=color, fill=fill_color)
        else:
            draw.rectangle(bbox, width=width, outline=color)

        if labels is not None:
            margin = width + 1
            draw.text((bbox[0] + margin, bbox[1] + margin), labels[i], fill=color, font=txt_font)

    return torch.from_numpy(np.array(img_to_draw)).permute(2, 0, 1).to(dtype=torch.uint8)