# -*- coding: utf-8 -*-
"""
Created on Tue Sep  7 17:02:05 2021

@author: User
"""

import os
import sys
from pathlib import Path

def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn

import platform
if not platform.node()=='DESKTOP-NQEQ4AE':
    os.environ['TORCH_HOME'] = os.path.join( Path( os.path.abspath(__file__) ).parent, 'pretrained_models' )
    sys.path.append( os.path.join( Path(os.path.abspath(__file__)).parent, 'misc/vision/references/detection' ) )

import json
import pickle
import numpy as np
import torch
from torch import nn
from tqdm import tqdm
from config import device, data_dir, output_dir, models_dict, batch_size, lr, epochs
from models import *

from argparse import ArgumentParser

from datautils import seed_everything, train_model, evaluate_model
SEED=42
seed_everything(SEED=SEED)

from datautils import vector_dataloader, transform_image
import joblib
from sklearn.metrics import classification_report
import matplotlib.pyplot as plt

parser = ArgumentParser()
parser.add_argument('--train', action='store_true')
parser.add_argument('--dummy', action='store_true')
parser.add_argument('--batch_size', type = int, default = batch_size )
parser.add_argument('--lr', type = float, default = lr )
parser.add_argument('--epochs', type = int, default = epochs )
parser.add_argument('--device', type = str, default = device )
parser.add_argument('--experiment', type = str, default = 'vectors_directions' )
parser.add_argument('--imagename', type = str, default = None, 
                     help = 'if imagename is defined, then a new image is classified.' )
parser.add_argument('--load_from_list', action = 'store_true' )
parser.add_argument('--training_size', type = int, default = None )
args = parser.parse_args()

model_name = f'CNN_paper_{args.experiment}_{args.batch_size}_{args.lr}_{args.epochs}.pt'

# TRAINING:
if args.train:
    train_dataloader = vector_dataloader(dataset='train', batch_size=args.batch_size, \
                                         experiment = args.experiment, load_from_list = args.load_from_list, \
                                             training_size = args.training_size, without_masks = True )
    
    model, train_loss_plt = train_model( 'vectordirection_regressor', train_dataloader, args.lr, args.epochs, loss_func=nn.MSELoss() )

    # save model:
    path = os.path.join( output_dir, args.experiment )
    if not os.path.isdir( path ):
        os.mkdir( path )
    torch.save( model, os.path.join( path, model_name ))
else:
    model = torch.load( os.path.join( output_dir, args.experiment, model_name ))

model.to(args.device)


#####################
# EVALUATION for the differen experiments:

# vectors directions:
if args.experiment=='vectors_directions':
    val_dataloader = vector_dataloader(dataset='val', experiment = args.experiment, \
                                       load_from_list = args.load_from_list, without_masks = True )
    
    if not args.imagename:
        if not args.dummy:
            (true_y,pred_vals) = evaluate_model( model, val_dataloader, experiment=args.experiment )
            true_y = true_y.reshape(1,-1)
            pred_vals = pred_vals.reshape(1,-1)
            
            pickle.dump( (true_y,pred_vals), open( os.path.join( output_dir, args.experiment, model_name.split('.')[0] + '.pickle' ), 'wb' ) )
            print( np.corrcoef(true_y,pred_vals)[0,1] )
            plt.scatter( true_y, pred_vals )
            plt.show()
            
        else:
            # with randomly initialized model:
            model = 'vectordirection_regressor'
            paras = {'output_dim': 1}
            model = models_dict( model )( **paras )
            model.to(device)
            
            (true_y,pred_vals) = evaluate_model( model, val_dataloader, experiment=args.experiment )
            true_y = true_y.reshape(1,-1)
            pred_vals = pred_vals.reshape(1,-1)
            
            print( np.corrcoef(true_y,pred_vals)[0,1] )
            
    else:
        X = transform_image( args.imagename, model_name )
        X = X.to( device ).unsqueeze(dim=0)
        
        softmax = nn.Softmax(dim=0)
        probs = softmax( model(X).squeeze() )
        probs = probs.detach().cpu().numpy()
        
        print( ''.join([ '{}: {:.2f}\n'.format(label,prob) for label,prob in zip(lb.classes_,probs) ]) )    


# shape detection:
elif args.experiment=='shape_detection':

    if not args.imagename:       
        if not args.dummy:
            
            lb = joblib.load( os.path.join( data_dir, 'shapes_paper_Korchi2020', 'lb.pkl' ) )
            
            lb = joblib.load( os.path.join( data_dir, 'shapes_paper_Korchi2020', 'lb.pkl' ) )
            val_dataloader = shapes_dataloader( dataset = 'val', batch_size = args.batch_size )
            (true_y,pred_vals) = evaluate_model( model, val_dataloader, lb.classes_ )
            
            file = f'classification_report_{args.batch_size}_{args.lr}_{args.epochs}.json'
            json.dump( classification_report(true_y, pred_vals, target_names=lb.classes_, output_dict = True ), open( os.path.join( output_dir, args.experiment, file ), 'w' ) )
            
        else:
            # with randomly initialized model:
            model = CNN_paper()
            model.to(device)
            
            val_dataloader = shapes_dataloader( dataset = 'val' )
            evaluate_model( model, val_dataloader, lb.classes_ )
                
    else:
        X = transform_image( args.imagename, model_name )
        X = X.to( device ).unsqueeze(dim=0)
        
        softmax = nn.Softmax(dim=0)
        probs = softmax( model(X).squeeze() )
        probs = probs.detach().cpu().numpy()
        
        print( ''.join([ '{}: {:.2f}\n'.format(label,prob) for label,prob in zip(lb.classes_,probs) ]) )
        

    