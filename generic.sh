#!/bin/bash

# This is a generic running script. It can run in two configurations:
# Single job mode: pass the python arguments to this script
# Batch job mode: pass a file with first the job tag and second the commands per line

#SBATCH --partition=all
#SBATCH --cpus-per-task=4
#SBATCH --mem=50G
#SBATCH --time=4-00:00

set -e # fail fully on first line failure

# Customize this line to point to conda installation
module load lang/Anaconda3
source activate vision_env

source init_env.sh

echo "Running on $(hostname)"

if [ -z "$SLURM_ARRAY_TASK_ID" ]
then
    # Not in Slurm Job Array - running in single mode

    JOB_ID=$SLURM_JOB_ID

    # Just read in what was passed over cmdline
    JOB_CMD="${@}"
else
    # In array

    JOB_ID="${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}"

    # Get the line corresponding to the task id
    JOB_CMD=$(head -n ${SLURM_ARRAY_TASK_ID} "$1" | tail -1)
fi

#SBATCH -o ./log/${JOB_ID}

# Train the model
srun python $JOB_CMD