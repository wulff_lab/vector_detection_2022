# -*- coding: utf-8 -*-
"""
Created on Fri Apr  2 09:05:58 2021

@author: User
"""

import numpy as np
import random

from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('--experiment', default='vectors_random', type=str )
args = parser.parse_args()

if args.experiment=='vectors_random':
    jobs = []
    for batch_size in [2,5,10]:
        for epoch in [2,10,30]:
            jobs.append( f'detecting_vectors.py --batch_size {batch_size} --epochs {epoch} --experiment {args.experiment}' )
    
    open( 'joblist.txt', 'w', encoding = 'utf-8' ).write('\n'.join( jobs ) )
elif args.experiment=='vectors_horizontal':
    jobs = []
    for batch_size in [2,5,10]:
        for epoch in [2,10,30]:
            jobs.append( f'detecting_vectors.py --batch_size {batch_size} --epochs {epoch} --experiment {args.experiment}' )
    
    open( 'joblist.txt', 'w', encoding = 'utf-8' ).write('\n'.join( jobs ) )