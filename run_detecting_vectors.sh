#!/bin/bash

# Expects to be in the same folder as generic.sh

# Edit this if you want more or fewer jobs in parallel
jobs_in_parallel=30

# get filenames of split file:
file=joblist.txt

n_lines=$(grep -c '^' "${file}")

# Use file name for job name
job_name=$(basename "${file}" .txt)

sbatch --array=1-${n_lines}%${jobs_in_parallel} --job-name ${job_name} generic.sh "${file}"