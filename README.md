# Repository for KI-bED project

In this README file you find information on first results, training procedure and hyperparameter evaluation.

## First results:

These findings are based on the pretrained model ```torchvision.models.detection.fasterrcnn_resnet50_fpn``` that was finetuned on a simulated dataset for 50 epochs with batch size of 3 (Raw image files can be found in ```data/testimages```).

### Example 1:

Own drawing of vectors on Tablet-PC that was fed into the finetuned Faster-RCNN prediction model: 

![out_vectors1](/uploads/a502be43d41781ebc6db23b0c3e2254a/out_vectors1.png)

(Note: green boxes refer to model predicted detection of vectors)


### Example 2:

![out_vectors3](/uploads/f7d8588c4246177fc57b10116948cfae/out_vectors3.png)


## Training models:

```
	python detect_vectors.py ...
	OR
	python regressing_directions.py ...
```

## Evaluating hyperparameters

```
	python hyperparameter_evaluation.py ...
```

Sources of other repos are indicated in the code files.
