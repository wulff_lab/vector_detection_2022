# -*- coding: utf-8 -*-
"""
Created on Sat Sep  4 14:24:11 2021

@author: User
"""

import torch
from torch import nn
from torch.nn import Module
from torch.nn import Conv2d
from torch.nn import Linear
from torch.nn import MaxPool2d
from torch.nn import ReLU
from torch.nn import LogSoftmax
from torch import flatten
import torch.nn.functional as F

class FeedForwardNet(nn.Module):
    # see: https://pytorch.org/tutorials/beginner/text_sentiment_ngrams_tutorial.html
    # better: https://pytorch.org/tutorials/beginner/nlp/word_embeddings_tutorial.html
    def __init__(self, input_dim, hidden_dim, output_dim ):
        super().__init__()
        
        self.linear = nn.Linear( input_dim, hidden_dim )
        self.output = nn.Linear( hidden_dim, output_dim )
        self.relu = nn.ReLU()
        # self.sigmoid = nn.Sigmoid()
        self.softmax = nn.Softmax( dim = 1 )
        
    def forward(self, x ):
        
        linear_layer = self.relu( self.linear( x ) )
        output = self.output( linear_layer )

        proba = self.softmax( output )

        return proba


class CNN(nn.Module):
    # source: https://pytorch.org/tutorials/recipes/recipes/defining_a_neural_network.html
    # old: https://www.pyimagesearch.com/2021/07/19/pytorch-training-your-first-convolutional-neural-network-cnn/ 
    def __init__(self):
        super(CNN, self).__init__()
        self.conv1 = nn.Conv2d(1, 32, 3, 1)
        self.conv2 = nn.Conv2d(32, 64, 3, 1)
        self.dropout1 = nn.Dropout2d(0.25)
        self.dropout2 = nn.Dropout2d(0.5)
        self.fc1 = nn.Linear(457856, 128)
        self.fc2 = nn.Linear(128, 2)

    # x represents our data
    def forward(self, x):
        # Pass data through conv1
        x = self.conv1(x)
        
        # Use the rectified-linear activation function over x
        x = F.relu(x)
        
        x = self.conv2(x)
        x = F.relu(x)
        
        # Run max pooling over x
        x = F.max_pool2d(x, 2)
        # Pass data through dropout1
        x = self.dropout1(x)
        # Flatten x with start_dim=1
        x = torch.flatten(x, 1)
        # Pass data through fc1
        x = self.fc1(x)
        x = F.relu(x)
        x = self.dropout2(x)
        x = self.fc2(x)
        
        # Apply softmax to x
        # output = F.log_softmax(x, dim=1)
        output = F.softmax(x, dim=1)
        return output

class CNN_paper(nn.Module):
    # model from: https://reader.elsevier.com/reader/sd/pii/S2352340920309847?token=9E8965C60E4FF6534F1D65F8F4AB8F9CFEA63F44419474043CA2757CA72E764424ACEC584A207461D18578D1B73279ED&originRegion=eu-west-1&originCreation=20210907145150
    # Korchi, A. E., & Ghanou, Y. (2020). 2d geometric shapes dataset - for machine learning and pattern recognition. Data in Brief, 32, 106090. https://doi.org/10.1016/j.dib.2020.106090
    def __init__(self, output_dim=9 ):
        super(CNN_paper,self).__init__()
        self.conv1 = nn.Conv2d(3, 16, kernel_size=(7,7), stride=(3,3))
        self.conv1_bn = nn.BatchNorm2d(16)
        self.elu = nn.ELU()
        self.max_pool = nn.MaxPool2d(kernel_size=(3,3), stride=(1,1))
        self.conv2 = nn.Conv2d(16, 32, kernel_size=(3,3), stride=(1,1))
        self.conv2_bn = nn.BatchNorm2d(32)
        self.max_pool2 = nn.MaxPool2d(kernel_size=(2,2), stride=(1,1))
        self.conv3 = nn.Conv2d(32, 32, kernel_size=(3,3), stride=(1,1))
        self.conv3_bn = nn.BatchNorm2d(32)
        self.conv4 = nn.Conv2d(32, 16, kernel_size=(3,3), stride=(1,1))
        self.conv4_bn = nn.BatchNorm2d(16)
        self.flatten = nn.Flatten(1,-1)
        self.linear = nn.Linear(46656,output_dim)
        self.softmax = nn.Softmax(dim=0)
        
    def forward(self,x):
        out_img = self.conv1_bn( self.conv1( x ) )
        out_img = self.elu( out_img )
        out_img = self.max_pool( out_img )
        out_img = self.conv2_bn( self.conv2( out_img ) )
        out_img = self.max_pool2( out_img )
        out_img = self.elu( out_img )
        out_img = self.conv3_bn( self.conv3( out_img ) )
        out_img = self.elu( out_img )
        out_img = self.max_pool2( out_img )
        out_img = self.conv4_bn( self.conv4( out_img ) )
        out_img = self.elu( out_img )
        out_img = self.max_pool2( out_img )
        
        out_img = self.flatten( out_img )
        out_img = self.linear( out_img )
        
        # it seems very crucial to omit this last normalization layer!!!
        # out_img = self.softmax(out_img)
        
        return out_img

class vectordirection_regressor(nn.Module):
    # model from: https://reader.elsevier.com/reader/sd/pii/S2352340920309847?token=9E8965C60E4FF6534F1D65F8F4AB8F9CFEA63F44419474043CA2757CA72E764424ACEC584A207461D18578D1B73279ED&originRegion=eu-west-1&originCreation=20210907145150
    # Korchi, A. E., & Ghanou, Y. (2020). 2d geometric shapes dataset - for machine learning and pattern recognition. Data in Brief, 32, 106090. https://doi.org/10.1016/j.dib.2020.106090
    def __init__(self, output_dim=9 ):
        super(vectordirection_regressor,self).__init__()
        self.adaptiv_pooling = nn.AdaptiveAvgPool2d((200,200))
        self.conv1 = nn.Conv2d(3, 16, kernel_size=(7,7), stride=(3,3))
        self.conv1_bn = nn.BatchNorm2d(16)
        self.elu = nn.ELU()
        self.max_pool = nn.MaxPool2d(kernel_size=(3,3), stride=(1,1))
        self.conv2 = nn.Conv2d(16, 32, kernel_size=(3,3), stride=(1,1))
        self.conv2_bn = nn.BatchNorm2d(32)
        self.max_pool2 = nn.MaxPool2d(kernel_size=(2,2), stride=(1,1))
        self.conv3 = nn.Conv2d(32, 32, kernel_size=(3,3), stride=(1,1))
        self.conv3_bn = nn.BatchNorm2d(32)
        self.conv4 = nn.Conv2d(32, 16, kernel_size=(3,3), stride=(1,1))
        self.conv4_bn = nn.BatchNorm2d(16)
        self.flatten = nn.Flatten(1,-1)
        self.linear = nn.Linear(46656,output_dim)
        self.softmax = nn.Softmax(dim=0)
        
    def forward(self,x):
        out_img = self.adaptiv_pooling(x)
        out_img = self.conv1_bn( self.conv1( out_img ) )
        out_img = self.elu( out_img )
        out_img = self.max_pool( out_img )
        out_img = self.conv2_bn( self.conv2( out_img ) )
        out_img = self.max_pool2( out_img )
        out_img = self.elu( out_img )
        out_img = self.conv3_bn( self.conv3( out_img ) )
        out_img = self.elu( out_img )
        out_img = self.max_pool2( out_img )
        out_img = self.conv4_bn( self.conv4( out_img ) )
        out_img = self.elu( out_img )
        out_img = self.max_pool2( out_img )
        
        out_img = self.flatten( out_img )
        out_img = self.linear( out_img )
        
        # it seems very crucial to omit this last normalization layer!!!
        # out_img = self.softmax(out_img)
        
        return out_img
