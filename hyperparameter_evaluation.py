# -*- coding: utf-8 -*-
"""
Created on Sat Sep 18 18:26:48 2021

@author: User
"""

from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('--experiment', default='vectors_random', type =str)
args = parser.parse_args()

from config import output_dir
import os 
import pandas as pd
import re
import numpy as np

base = os.path.join( output_dir, args.experiment )
filepaths = [ os.path.join( base, filename ) for filename in os.listdir( base ) if filename.startswith('slurm') ]

if args.experiment=='vectors_random':
    jobs = []
    for batch_size in [2,5,10]:
        for epoch in [2,10,30]:
            jobs.append( f'detecting_vectors.py --batch_size {batch_size} --epochs {epoch} --experiment {args.experiment}' )
    for batch_size in [2,5,10]:
        for epoch in [0]:
            jobs.append( f'detecting_vectors.py --batch_size {batch_size} --epochs {epoch} --experiment {args.experiment}' )
    
elif args.experiment=='vectors_horizontal':
    jobs = []
    for batch_size in [2,5,10]:
        for epoch in [2,10,30]:
            jobs.append( f'detecting_vectors.py --batch_size {batch_size} --epochs {epoch} --experiment {args.experiment}' )

hyperparameters = ['batch_size','epochs']
df = pd.DataFrame({'batch_size': [], 'epochs': [], 'p': [], 'r': []})
for job in jobs:
    df_new = pd.DataFrame({'batch_size': [], 'epochs': [], 'p': [], 'r': []})
    for hyperpara in hyperparameters:
        value = [ x.split()[1] for x in job.split('--') if hyperpara in x ]
        df_new[hyperpara] = value
        
    df = pd.concat([df,df_new])
df.set_index( np.arange(0,len(df)), inplace=True)

metrics_dict = {'p': 'Precision','r': 'Recall'}
for n,filepath in enumerate(filepaths):
    for metric_key,metric in metrics_dict.items():
        text = open( filepath, 'r' ).read()
        
        metric_search_str = f'Average {metric}'
    
        try:
            end_idx = list( re.finditer(metric_search_str, text ) )[4].end()
            value = float( re.search( r'[01]\.[0-9]{3,}', text[ end_idx:end_idx+100] ).group(0) )
            df.loc[n,metric_key] = [value]
        except:
            continue

print( df )