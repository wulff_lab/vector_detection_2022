# -*- coding: utf-8 -*-
"""
Created on Thu Sep  9 17:19:03 2021

@author: User

source: https://pytorch.org/tutorials/intermediate/torchvision_tutorial.html
"""

import sys

# this is for solving weird url loading error with pytorch on HPC
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

import os
from pathlib import Path
import platform

from config import device, pretrained_models_dir
from datautils import vector_dataloader, get_model_instance_segmentation
from engine import train_one_epoch, evaluate

if not platform.node()=='DESKTOP-NQEQ4AE':
    os.environ['TORCH_HOME'] = os.path.join( Path( os.path.abspath(__file__) ).parent, 'pretrained_models' )
    sys.path.append( os.path.join( Path(os.path.abspath(__file__)).parent, 'misc/vision/references/detection' ) )

import numpy as np

import torch

from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('--epochs', default = 3, type = int )
parser.add_argument('--batch_size', default = 3, type = int )
parser.add_argument('--pretrained', action='store_true' )
parser.add_argument('--load_from_list', action='store_true' )
parser.add_argument('--training_size', default=None, type = int )
parser.add_argument('--experiment', default = 'vectors', type=str )
args = parser.parse_args()

num_classes = 2 # 1 vector + background
# device = 'cpu'

# get the model using our helper function
model = get_model_instance_segmentation(num_classes,args.pretrained)

train_dataloader = vector_dataloader(dataset='train', batch_size=args.batch_size, experiment = args.experiment, load_from_list = args.load_from_list, training_size = args.training_size)
val_dataloader = vector_dataloader(dataset='val', experiment = args.experiment, load_from_list = args.load_from_list)
small_val_dataloader = vector_dataloader(dataset='val', experiment = args.experiment, load_from_list = args.load_from_list, abs_size_val = 100 )

# move model to the right device
model.to(device)

# construct an optimizer
params = [p for p in model.parameters() if p.requires_grad]
optimizer = torch.optim.SGD(params, lr=1e-5,
                            momentum=0.9, weight_decay=0.0005)

from torch.optim import Adam
optimizer = Adam(params, lr = 1e-6 )

# and a learning rate scheduler
lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer,
                                               step_size=3,
                                               gamma=0.1)

for epoch in range(args.epochs):
    # train for one epoch, printing every 10 iterations
    train_one_epoch(model, optimizer, train_dataloader, device, epoch, print_freq=10)
    # update the learning rate
    lr_scheduler.step()

save_dir = os.path.join( pretrained_models_dir, args.experiment )
if not os.path.isdir( save_dir ):
    os.mkdir( save_dir )
torch.save( model, os.path.join( save_dir, f'finetuned_fasterrcnn_resnet50_fpn_{args.experiment}_{args.epochs}_{args.batch_size}.pt' ))
# torch.save( model.state_dict(), os.path.join( save_dir, f'finetuned_fasterrcnn_resnet50_fpn_{args.experiment}_{args.epochs}_{args.batch_size}_state_dict.pt' ))

model.to('cpu')
# small evaluate on the test dataset
evaluate(model, small_val_dataloader, device='cpu')

# evaluate on all
evaluate(model, val_dataloader, device='cpu')
