# -*- coding: utf-8 -*-
"""
Created on Sat Sep  4 11:49:27 2021

@author: User
"""

import torch
from pathlib import Path
import os

from models import *

# parameters:
batch_size = 15
epochs = 10
lr = 5e-5

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

base = Path( os.path.abspath( __file__ ) ).parent

data_dir = os.path.join( base, 'data' )
pretrained_models_dir = os.path.join( base, 'pretrained_models' )
output_dir = os.path.join( base, 'output' )
if not os.path.isdir( output_dir ):
    os.mkdir( output_dir)

labels = ['circle','line']

# define models:
def models_dict( name, **kwargs ):
    if name=='FFNN':
        return FeedForwardNet(
                    input_dim = 30000,
                    hidden_dim = 1000,
                    output_dim = len(labels)
                )
    elif name=='CNN':
        return CNN()
    elif name=='CNN_paper':
        return CNN_paper
    elif name=='vectordirection_regressor':
        return vectordirection_regressor