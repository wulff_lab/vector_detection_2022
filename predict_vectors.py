# -*- coding: utf-8 -*-
"""
Created on Wed Sep 15 09:44:07 2021

@author: User
"""
import os

import torch
import torchvision
from torchvision.transforms import functional as F

from config import pretrained_models_dir, data_dir, device
from datautils import draw_bounding_boxes, get_model_instance_segmentation

import numpy as np
import matplotlib.pyplot as plt

from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('--image_name', default = None, type=str )
parser.add_argument('--epochs', default = 1, type=int )
parser.add_argument('--batch_size', default = 5, type=int )
parser.add_argument('--objects', default = 5, type=int )
parser.add_argument('--experiment', default = 'vectors_horizontal', type=str )
args = parser.parse_args()

if args.experiment=='vectors':
    model = torch.load( os.path.join( pretrained_models_dir, f'finetuned_maskrcnn_resnet50_fpn_20_5.pt' ))
else: 
    base = os.path.join( pretrained_models_dir, f'{args.experiment}' )
    model = torch.load( os.path.join( base, f'finetuned_fasterrcnn_resnet50_fpn_{args.experiment}_{args.epochs}_{args.batch_size}.pt' ))
    model.eval()


# print( f'test: {model( torch.randn(1,3,300,200).to(device) )}' )

def show(imgs):
    if not isinstance(imgs, list):
        imgs = [imgs]
    fix, axs = plt.subplots(ncols=len(imgs), squeeze=False)
    for i, img in enumerate(imgs):
        img = img.detach()
        img = F.to_pil_image(img)
        axs[0, i].imshow(np.asarray(img))
        axs[0, i].set(xticklabels=[], yticklabels=[], xticks=[], yticks=[])
    
    plt.show()
    

from PIL import Image
root = os.path.join(data_dir, 'testimages')
if not args.image_name:
    imgs = [ file for file in os.listdir(root) \
                if '.png' in file and \
                not file.replace('.png','').endswith('mask') and \
                not 'prototype' in file ]
    img = imgs[np.random.randint(0,len(imgs))]
else:
    img = f'{args.image_name}.png'

img_path = os.path.join(root, img)
img = Image.open(img_path).convert("RGB")


input_ = F.to_tensor(img).unsqueeze(dim=0).to(device)
input_nonnormalized = torch.tensor( input_.clone().cpu().detach()*255, dtype= torch.uint8 )

output = model( input_ )

box_idx = np.argsort( output[0]['scores'].cpu().detach().numpy() )[-args.objects:]

boxes = output[0]['boxes'][box_idx]
colors = [ 'green' for _,_ in enumerate(boxes) ]

show( draw_bounding_boxes(input_nonnormalized[0], boxes, colors=colors, width=5) )